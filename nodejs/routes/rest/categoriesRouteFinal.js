var express = require('express');
var router = express.Router();

// GET list of all events
router.get('/', function(req, res, next) {
    res.json(req.services.categories.listAll());
});

// GET list of all events of a categorie
router.get('/:catId/events', function(req, res, next) {
  let catId = req.params.catId;

  res.status(200).send(catId);
});

module.exports = router;
