// external module dependencies
let express = require('express');
let path = require('path');
let bodyParser = require('body-parser');
let moment = require('moment');

// internal module dependencies
let routes = require('./routes/');
let serviceMiddleware =  require('./middleware/serviceMiddleware')

// create express server
let app = express();

// add jade as view engine & tell express where look for templates
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
// add momentJS to our app to handle dates with javascript
app.locals.moment = moment;
// register cookieParser & bodyParser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(serviceMiddleware())
app.use('/node_modules', express.static(path.join(__dirname, 'node_modules')));
app.use(express.static(path.join(__dirname, 'public')));


app.get('/helloworld', (req, res) => res.send('Hello World!') );
app.get('/', (req,res) => res.render('firstPage'));
app.use('/generated', routes.htmlGenerator);
app.use('/api', routes.restRoutes);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// error handlers for development & production

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use((err, req, res, next) =>{
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use((err, req, res, next) =>{
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
