#!/usr/bin/env node
// shebang/magic line

// module dependencies
let app = require('./app');
let http = require('http');

// get port from environment and store in Express
let port = normalizePort(process.env.PORT || '8080');
app.set('port', port);

// create HTTP server.
let server = http.createServer(app);
require('./chat')(server);

// start HTTP server,
server.listen(port);

// HTTP server events: listening, error
server.on('listening', () =>
  console.log('Listening on port ' + server.address().port)
);
server.on('error', (err) =>{
  if (error.syscall !== 'listen') {
    throw err;
  }

  let bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw err;
  }
});


// helper function to normalize the port
function normalizePort(val) {
  let port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}
