let DBManager = require('./DBManager');
let Event = require('../model/event');
let db, categorieCollection;

// export function which creates our categoriesService object
module.exports = function categoriesService(dbURL) {
   DBManager.getConnection(dbURL, (database) => {
    db = database;
    categorieCollection = db.collection('categories');
  });
  // gets all categories
  categoriesService.listAll = (callback) => {
    categorieCollection.find({}).toArray( (err, array) => {
      if (err) throw err;
      callback(array);
    })
  }
  return categoriesService;
}
