import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appEnlarge]'
})
export class EnlargeDirective {

  constructor(private element: ElementRef) { }

  @Input('appEnlarge') zoomValue: number;

  private enlarge(value: number): void {
    this.element.nativeElement.style.fontSize = value + '%';
  }

  @HostListener('mouseenter') onMouseEnter(){
    this.enlarge(this.zoomValue);
  }

  @HostListener('mouseleave') onMouseLeave(){
    this.enlarge(100);
  }

}
