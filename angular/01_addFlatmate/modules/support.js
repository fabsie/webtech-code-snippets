class Supporter{
    isUndefinedOrEmpty(variable){
        if(variable === undefined || variable === ''){
            return true;
        }
        return false;
    };
    
    findID(array, id){
        var res;
        array.forEach(function(elem){
            if(elem.id === id){
                res = elem;
            }
        });
        return res;
    }
}

module.exports = Supporter;