import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appTempInfo]'
})
export class TempInfoDirective {

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainerRef: ViewContainerRef
  ) {
        this.viewContainerRef.createEmbeddedView(this.templateRef);
  }

  @Input('appTempInfo') set removeDelay(time: number) {
    setTimeout(
      () => {
        this.viewContainerRef.clear();
      },
      time);
  }

}
